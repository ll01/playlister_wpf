﻿using System;
using MahApps.Metro.Controls;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Playlister
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void MetroWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }

        private void MetroWindow_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void MetroWindow_SizeChanged(object sender, SizeChangedEventArgs e)
        {

        }

        private void Search_TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Auto_Play_Playlist_Check_Box_IsCheckedChanged(object sender, EventArgs e)
        {

        }

        private void ScrollViewer_MouseWheel(object sender, MouseWheelEventArgs e)
        {

        }

        private void Feeling_Lucky_Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Main_Tab_Control_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Music_Folder_Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Playlist_Folder_Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Playlist_Lenght_Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Rescan_Music_Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
